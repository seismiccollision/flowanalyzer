# The Wishlist
These are the features that would be worthwhile long-term for the Flow Analyzer project, but either require legal answers or additional resources (time, testing devices, etc) to move forward with.

## Geo-IP Lookup
It would be very useful to correlate public IP addresses with service providers and physical locations. See Issue #39 for additional information. 

## Scaling Documentation
Documentation on scaling Flow Analyzer implementation, including load-balancing for organizations with very large traffic volumes.

# ---
**Copyright (c) 2017, Manito Networks, LLC**
**All rights reserved.**